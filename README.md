# POSTGRESSQL


## POSTGRESQL INSTALLATION ON MACOS

Se recomienda el uso de Homebrew para instalar y gestionar aplicaciones en MacOS. Se instala utilizando el siguiente comando en la terminal de MacOS:

`/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`

The terminal runs through a series of installation operations, and will probably create folders in your local machine to accommodate Homebrews storage requirements. You can find more detailed instructions here. After it's installed, update the Homebrew dependencies and install PostgreSQL on the command line:

`brew update`  
`brew install postgresql`

Next, check your PostgreSQL version:

`postgres --version`  
`postgres (PostgreSQL) 12.2`

The command line results will show the version you have installed on your local machine. I recommed using the latest version of libraries and software whenever possible to avoid compatibility issues with client-side applications.

## HOW TO CREATE A PHYSICAL POSTGRESQL DATABASE

Now you can initialize the physical space on your hard-disk to allocate databases. To do this, create a default postgres database on the command line in case it didn't happen automatically:

`initdb /usr/local/var/postgres`

You will see the error message: "initdb: directory "/usr/local/var/postgres" exists but is not empty" if the database was already created when you installed PostgreSQL. It means the folder where you are attempting to create a physical place for the database already has one. Either way, next you can move on to the next step.

When you connect to this physical database later, you will see an actual database which is called "postgres" as well. The postgres database is meant to be the default database for any third-party tools that you are using in combination with PostgreSQL. These tools attempt to make the default connection to this default database, so you shouldn't delete it.

## HOW TO START/STOP A POSTGRESQL DATABASE

Let's see next how you can interact with the actual database. Manually start and stop your Postgres database server with the following commands:

`pg_ctl -D /usr/local/var/postgres start`  
`pg_ctl -D /usr/local/var/postgres stop`

The terminal will confirm these operations with "server started" and "server stopped" feedback. You could also implement a script to start the server each time you boot up the machine, but I like to have control over when to start and stop my database server to avoid complications.

## COMO CREAR UNA BASE DE DATOS POSTGRESQL

A continuación, veamos los pasos para configurar una base de datos que se pueda usar para una de sus aplicaciones. Asegúrese de que el servidor Postgre se inicie primero, luego escriba estos comandos en la línea de comandos para crear y eliminar una base de datos:

`createdb mydatabasename`  - Crea la base de datos
`dropdb mydatabasename` - Elimina la base de datos

También puede conectarse a bases de datos para ejecutar sentencias SQL. Utilice el comando psql o especifique una base de datos como la base de datos postgres predeterminada para conectarse:

`psql mydatabasename` 
`plsql -u username -d mydatabase` - Conectarse con un usuario dado a una base de datos dada

El comando lo lleva al shell psql, que puede salir escribiendo CTRL + d. En el shell psql, también puede crear y soltar bases de datos:

`CREATE DATABASE mydatabasename;`  
`DROP DATABASE mydatabasename;`

To list all your databases, you can type \list. Your will see any new databases listed, as well as two default databases that come with postgreSQL called template0 and template1. The templates should remain in your database list even if you aren't using them, as they may be useful later.

`\list` - Lista todas las base de datos existentes.  
`\c mydatabasename` - Se conecta a una base de datos.  
`\d - List` Lista la relacion existente en una base de datos especifica.  
`\d mytablename` - Muestra informacion sobre una table especifica.  
`\du` - Lista los usuarios en el motor de base de datos.  
`\q` - Sale de la consola PostgreSQL.  
`\h` - Muestra una lista de comandos. 
`\i ubicacion\nombre_fichero.sql` - Carga un fichero para su ejecucion


## CREATE USER

`CREATE USER nombreusuario;` - Crea un usuario

## COMANDO PARA LA CREACION DE USUARIOS

`CREATE USER nombreusuario WHIT comando_opcional;` - Crea un usuario y asigna privilegios especiales

`PASSWORD` - Asigna un password a un usuario creado  
`ENCRYPTED PASSWORD` - Asigna un password encriptado a un usuario creado  
`UNENCRYPTED PASSWORD` - Asigna una contraseña no encriptada a un usuario creado  
`VALID UNTIL` - valida una fecha de expiracion para un usuario creado  
`CREATEDB` - Otorga privilegios para crear BD  
`NOCREATEDB` -  No permite al usuario crear BD  
`SUPERUSER` - Otorga privilegios de superuser  
`NOSUPERUSER` -  No otorga privilegios de superuser  

## COMANDO PARA ELIMINAR USUARIOS

`DROP USER nombreusuario` - Elmina un usuario

## COMANDO PARA MODIFICAR USUARIOS

`ALTER USER nombreusuario comando_especial` - Perimte modificar un usuario para asignar privilegios especiales

## CONSULTAS

`SELECT * FROM nombre_tabla;` - Muestra el contenido de todas las columnas de la tabla. 
`SELECT columna FROM nombre_tabla;` - Muestra el contenido de la columna especificada en la tabla.
`SELECT columna1, columna2, columnan FROM nombre_tabla;` - Muestra el contenido de las columnas de la tabla.
`SELECT * FROM nombre_tabla where condicion;` - Muestra el contenido de todas las filas de una tabla dada una condicion.
`SELECT LENGTH(columna) AS nueva_columna FROM nombre_tabla where condicion;` - Permite ser usado para mostrar resultados en base a nuevas filas utilizando un select y ciertos criterios. 

`SELECT * FROM user;` - Indica el usuario que esta conectado.   
`DROP DATABASE mydatabasename` - Elimina una base de datos.


## COMANDO PARA LAS CONSULTAS

`AS` - Permite ser usado para mostrar resultados en base a nuevas filas utilizando un select y ciertos criterios. 
`GROUP BY` 
`ORDER BY ASC`
`ORDER BY DESC`
`LIMIT N` - Limita el resultado a N (N como un entero) 
`LENGTH(columna)` - Indica el largo del valor contenido en la columna
`IN`
`OR`
`AND`

## CREACION DE TABLAS

    CREATE TABLE nombre_tabla(  
        columna1 tipo_dato1,  
        columna2 tipo_dato2,  
        columnaN tipo_datoN,  
        PRIMARY KEY (columnaN)  
    ); 

    CREATE TABLE nombre_tabla1(  
        columna1 tipo_dato1,  
        columna2 tipo_dato2,  
        columnaN tipo_datoN,  
        FOREING KEY (columnaN) REFERENCES  
        nombre_tabla1 (columnaN)  
    ); 
    
## INSERCION DE DATOS 

    INSERT INTO nombre_tabla(  
        columna1,  
        columna2,  
        columnaN)
    VALUES(
        valor1,
        valor2,
        valor3
    ); 

    \copy nombre_tabla FROM 'directorio/archivo.csv' csv [header];
    

## ACTUALIZACION DE DATOS

    UPDATE nombre_tabla 
    SET  
        columna1=valor_nuevo1,  
        columna2=valor_nuevo2,  
        columnaN=valor_nuevoN
    WHERE condicion;
    

## ELIMINAR DATOS

    DELETE FROM nombre_tabla; 
    DELETE FROM nombre_tabla WHERE condicion;
    
## MODIFICANDO UNA TABLA

    ALTER TABLE nombre_tabla ADD columna_nueva tipo_dato;
    ALTER TABLE nombre_tabla DROP columnaN tipo_datoN;
    
## RESTRICCIONES AL CREAR TABLAS

- NOT NULL : Columna no puede contener valores nulos
- UNIQUE : Todos los valores de la columna deben ser diferentes unos con otros
- PRIMARY KEY: Clave primaria
- FOREIGN KEY: Clave foranea
- CHECK: Todos los valores de una columna deben satisfacer una condicion en especifico
- DEFAULT: Le da un valor por defecto a aquellos registros que no tengan un valor asignado
- INDEX: Crear y recuperar informacion de manera mas rapida mediante indices

## CREACION Y CONSULTA DE INDICES

`CREATE INDEX nombre_indice ON nombre_tabla(columna);`
`SELECT * FROM pg_indexes WHERE tablename='nombre_tabla';`
`DROP INDEX nombre_indice;`

## UNION ENTRE TABLAS

`SELECT columnas FROM tabla_A INNER JOIN B ON A.columna=B.columna;` - INTERSECCIÓN (existen en A y B)
`SELECT columnas FROM tabla_A LEFT JOIN B ON A.columna=B.columna;` - (Todos los registros de A + la INTERSECCION de A y B)
`SELECT columnas FROM tabla_A LEFT JOIN B ON A.columna=B.columna where B.columna IS NULL;` - (Todos los registros de A + la interseccion de A+B donde los registros de B sean nulos)
`SELECT columnas FROM tabla_A FULL OUTER JOIN B ON A.columna=B.columna;` (Todos los registros de A+B)
`SELECT columnas FROM tabla_A FULL OUTER JOIN B ON A.columna=B.columna where A.columna IS NULL OR B.columna IS NULL;` (Todos los registros de A+B - la interseccion)

## SUBQUERY

    SELECT columna1, columna2, columnaN
    FROM nombre_tabla
    WHERE columna1 IN
        (SELECT columna1
         FROM nombre_tabla2
         WHERE condicion);
         
         
## TRANSACCIONES

Aplican a los INSERT,UPDATE y DELETE

- BEGIN : El sistema permite que se ejecuten todas las sentencias SQL que necesitemos.
- COMMIT : Guarda los cambios de la transaccion.
- ROLLBACK : Retrocede los cambios realizados.
- SAVEPOINT : Guarda el punto de partida al cual volver a la hora de aplicar ROLLBACK
- SET TRANSACTION : Le asigna nombre a la transaccion.

## DUMP

`pg_dump -U nombre_usuario nombre_db > db.out`
`pg_dumpall >dumpll.sql`


## RESTAURAR

`psql -U postgres nombre_db < archivo_restauracion.sql`
`psql -f dumpall.sql mydb`